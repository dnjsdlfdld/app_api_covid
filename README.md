# 공공 데이터 API 활용 APP
### 코로나 선별 진료소
***
### LANGUAGE
```
* Dart
* Flutter
```
***
### 기능
```
    - 공공 데이터 포털에서 API 사용
    - 코로나 선별 진료소 리스트 가져오기
    - 무한 스크롤 기능
    - component 활용
    - 지역 리스트 생성하여 지역별 검색 구현
    - List의 countView 기능
```
![covid_clinic](./assets/covid_clinic.png)