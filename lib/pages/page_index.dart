import 'package:app_api_test4/components/component_count_title.dart';
import 'package:app_api_test4/components/component_list_item.dart';
import 'package:app_api_test4/components/component_no_data.dart';
import 'package:app_api_test4/model/hospital_list_item.dart';
import 'package:app_api_test4/repository/repo_hospital_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _scrollController = ScrollController();

  final _formKey = GlobalKey<FormBuilderState>();
  bool _areaHasError = false;

  var areaOptions = ['전체', '서울특별시', '경기도 안산시', '인천광역시 연수구 송도'];

  List<HospitalListItem> _list = [];
  int _page = 1; // 현재페이지
  int _totalPage = 1; // 총 페이지 개수
  int _perPage = 10; // 한 페이지 당 보여지는 아이템 개수
  int _totalCount = 0; // 총 아이템 개수
  int _currentCount = 0; // 현재 보여지고 있는 아이템의 번호
  int _matchCount = 0; // 검색 결과 개수
  // 총 페이지 개수를 알 수 없으므로 검색 결과 개수 / perPage 를 하여 총 페이지 개수를 올림 형식으로 만든다.
  String _searchArea = '전체';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      } // 스크롤이 마지막에 닿으면 loadItems 를 한 번 더 실행해
    });
    _loadItems();
  }
  // void 인 이유는 위 메소드로 보내고 끝내버릴거임
  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) { // 비우고 다시 가져오기 때문에 이 위치임 / 초기화 값으로 세팅
      _list = [];
      _page = 1;
      _totalPage = 1;
      _perPage = 10;
      _totalCount = 0;
      _currentCount = 0;
      _matchCount = 0;
    }

    if(_page <= _totalPage) { // 현재 페이지가 총 페이지보다 크면 안 되기에 조건을 걸어줌
      await RepoHospitalList()
          .getList(page: _page, searchArea: _searchArea)
          .then((res) => {   // result 의 약자 res/ 성공하면
        setState(() {
          _totalPage = (res.matchCount / res.perPage).ceil();
          _totalCount = res.totalCount;
          _currentCount = res.currentCount;
          _matchCount = res.matchCount;
          _list = [..._list, ...?res.data]; // 추가가 계속 되는거고, 배열의 길이가 계속 늘어난다.

          _page++;
        })
      })
          .catchError((err) => {
        debugPrint(err)
      });
    }

    if (reFresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
      // 어디에위치? , 1000milliseconds 가 1초 , 모션
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.brown,
        title: const Text('Covid19 진료소'),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody()
        ],
      ),
      bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: FormBuilderDropdown<String>(
              // autovalidate: true,
              name: 'area',
              decoration: const InputDecoration(
                labelText: '지역검색',
                hintText: '지역선택',
              ),
              items: areaOptions
                  .map((area) => DropdownMenuItem(
                alignment: AlignmentDirectional.center,
                value: area,
                child: Text(area),
              ))
                  .toList(),
              onChanged: (val) {
                setState(() {
                  _searchArea = _formKey.currentState!.fields['area']!.value!; // ! 필수
                  _loadItems(reFresh: true);
                });
              },
              valueTransformer: (val) => val?.toString(),
            ),
          )),
    );
  }

  Widget _buildBody() {
    if (_matchCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min, // 내용물만큼만 사이즈 잡어
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(icon: Icons.accessibility_outlined, count: _matchCount, unitName: '건', itemName: '병원'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentListItem(
                item: _list[index],
                callback: () {

                }),
          )
        ],
      );
      // return SingleChildScrollView(
      //   child: Column(
      //     children: [
      //       for (int i = 0; i < _list.length; i++)
      //         Text('${_list[i].orgnm} ${_list[i].lunchSttTm} ${_list[i].lunchEndTm}'),
      //     ],
      //   ),
      // );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 50, // appBar 높이를 빼줘야 한다. 전체가 SizedBox 가 된다.
        child: const ComponentNoData(icon: Icons.icecream_outlined, msg: '데이터 없음'),
      );
    }
  }
}