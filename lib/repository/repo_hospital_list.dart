import 'package:app_api_test4/config/config_api.dart';
import 'package:app_api_test4/model/hospital_list.dart';
import 'package:dio/dio.dart';

class RepoHospitalList { // 어디 가야하는지 지정해줄거다 API
  // End Point 지정해줬다
  final String _baseUrl = 'https://api.odcloud.kr/api/apnmOrg/v1/list?page={page}&perPage={perPage}&serviceKey=ffzbt251LaJ%2F7rHLJrCbVET2d8hg7FDVqSHWJ1HtP9TwtJF%2Fyr20ixpa73bbbQwgZYp8br1jyNhYF9aUc6rEGQ%3D%3D';

  // 미래에 이거 들어올거야  비동기 방식으로 해야 한다. 서버터진다
  Future<HospitalList> getList({int page = 1, int perPage = 10, String searchArea = '전체'}) async { // 값을 주지 않으면 1, 10으로 먹는다
    String _resultUrl = _baseUrl.replaceAll('{page}', page.toString());
    _resultUrl = _resultUrl.replaceAll('{perPage}', perPage.toString());
    if (searchArea != '전체') {
      _resultUrl = _resultUrl + '&cond%5BorgZipaddr%3A%3ALIKE%5D=${Uri.encodeFull(searchArea)}';
    }

    // Map<String, dynamic> params = {};
    // params['page'] = page;
    // params['serviceKey'] = Uri.encodeFull(apiCovidKey);
    //
    // if (searchArea != '전체') {
    //   params['cond%5BorgZipaddr%3A%3ALIKE%5D'] = Uri.encodeFull(searchArea);
    // }

    Dio dio = Dio();

    // async 와 await 는 짝궁이다 기다려주겠다
    final response = await dio.get(
        _resultUrl,
        options: Options(
          // 가라고 한 곳으로만 가! 다른데 가자고 하면 안된다고해
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }
        )
    );
    // 200번일때만
    return HospitalList.fromJson(response.data);
  }
}