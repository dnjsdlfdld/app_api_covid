import 'package:flutter/material.dart';

class ComponentNoData extends StatelessWidget {
  final IconData icon;
  final String msg;

  const ComponentNoData({super.key, required this.icon, required this.msg});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
              icon, size: 60,
          ),
          SizedBox(height: 30),
          Text(msg, style: TextStyle(fontSize: 14, letterSpacing: 1.0))
        ],
      ),
    );
  }
}
