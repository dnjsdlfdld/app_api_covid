import 'package:app_api_test4/model/hospital_list_item.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({
    super.key,
    required this.item,
    required this.callback
  });

  final HospitalListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 10, left: 12, right: 12, top: 20),
        decoration: BoxDecoration(
            color: Colors.white12,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(color: const Color.fromRGBO(100, 20, 100, 20,))
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${item.orgnm == null ? '병원 이름이 없습니다.' : item.orgnm}',
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 17,
                letterSpacing: 1.0
              ),),
            Row(
              children: [
                Icon(Icons.phone, size: 16,),
                SizedBox(width: 5,),
                Text('${item.orgTlno == null ? '연락처가 없습니다.' : item.orgTlno}', style: TextStyle(fontSize: 15),),
              ],
            ),
            Text('주소: ${item.orgZipaddr == null ? '등록된 주소가 없습니다. 병원에 문의하세요' : item.orgZipaddr}'),
            Text('휴무일 여부: ${item.hldyYn == null ? '등록된 정기 휴무가 없습니다.' : item.hldyYn}')
          ],
        ),
      ),
    );
  }
}
