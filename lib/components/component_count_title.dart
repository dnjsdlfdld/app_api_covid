import 'package:flutter/material.dart';

class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({
    super.key,
    required this.icon,
    required this.count,
    required this.unitName,
    required this.itemName});

  final IconData icon;
  final int count;
  final String unitName;
  final String itemName;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 12),
      child: Row(
        children: [
          Icon(icon, size: 30),
          const SizedBox(width: 10),
          Text(
            '총 ${count.toString()} $unitName의 $itemName이 있습니다.',
            style: TextStyle(
                letterSpacing: 1.2,
                fontSize: 16,
                fontWeight: FontWeight.w500),)
        ],
      ),
    );
  }
}
